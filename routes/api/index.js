﻿const config = require('./../../libs/config');
const log = require('./../../libs/logs')(module);
const adres = config.adress;
module.exports = function (req, res) {
    res.render('api', {
        mas: [
            {
                url: adres + "/moreSlider",
                type: "POST",
                param: "{\n  _id\n  num\n  Name\n  type1\n  hoverTxt\n  foto\n  }",
                result: "String",
                description: "Создает новый элемент в слайдере под номером num(нумерация с 1), структура slide:{\n  Name\n  type\n  hoverTxt\n}\n Name и type1 - строки, hoverTxt - массив",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/addInstitution",
                type: "POST",
                param: "{\n  type\n  mainTitle\n  address\n  coordinates\n  area\n  schoolNear\n  foto\n}",
                result: "String",
                description: "Создает новое учреждение",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/addText",
                type: "POST",
                param: "{\n  _id\n  text\n  title\n}",
                result: "String",
                description: "Находит по _id заведение и создает новый текст, по тому что будет прислано в полях text и title",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/sort/getById",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "[\n  {\n    _id\n    type\n    mainImg\n    mainTitle\n    address\n    text:{\n      title\n      text\n    }\n    coordinates\n    area\n    schoolNear:[1,2,3]\n    slider_1:{\n      title\n      slides:\n      [{\n        Name\n        type\n        hoverTxt\n        img\n      }]\n    }\n    slider_2\n    slider_3\n    slider_4\n    slider_5\n    slider_6\n    slider_7\n  }\n]",
                description: "Выдает одно учреждение по _id",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/sort/getAll",
                type: "GET",
                param: "{\n  _id\n  num\n  slide\n  foto\n  }",
                result: "[\n  {\n    _id\n    type\n    mainImg\n    mainTitle\n    address\n    text:{\n      title\n      text\n    }\n    coordinates\n    area\n    schoolNear:[1,2,3]\n    slider_1:{\n      title\n      slides:\n      [{\n        Name\n        type\n        hoverTxt\n        img\n      }]\n    }\n    slider_2\n    slider_3\n    slider_4\n    slider_5\n    slider_6\n    slider_7\n  }\n]",
                description: "Выдает все учреждения",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/delSlide",
                type: "POST",
                param: "{\n  _id\n  id\n  slide_id\n  num\n  }",
                result: "String",
                description: "Удаляет элемент слайдера. _id-_id заведения, id - id слайда, slide_id - _id слайда, num - номер слайдела",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/editInstitution",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "String",
                description: "Имена полей для изменений такие же, как и в БД, изменять можно только первые ключи(после одной точки).\n",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/status/true/byId",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "String",
                description: "Делает учреждение видимым в поиске",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/status/false/byId",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "String",
                description: "Делает учреждение невидимым в поиске",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/admin/getAll",
                type: "POST",
                param: "{}",
                result: "[]",
                description: "Выдает все учреждения, даже те, которые не видны",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/admin/getById",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "Схема внизу",
                description: "Выдает элемент по id",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/admin/getWithPar",
                type: "POST",
                param: "{\n  _id\n  }",
                result: "Схема внизу",
                description: "Выдает элемент по id",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/editSlide",
                type: "POST",
                param: "{\n  _id\n  id\n  num\n  \nполя-замены  }",
                result: "String",
                description: "исменяет слайд _id-_id заведения\n id-id слайда,\nnum - номер слайдера, \n поля сдайда для замены и фото",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/sort/getWithPar",
                type: "POST",
                param: "{\n  title\n  area\n  type\n  }",
                result: "[\n  {\n    _id\n    type\n    mainImg\n    mainTitle\n    address\n    text:{\n      title\n      text\n    }\n    coordinates\n    area\n    schoolNear:[1,2,3]\n    slider_1:{\n      title\n      slides:\n      [{\n        Name\n        type\n        hoverTxt\n        img\n      }]\n    }\n    slider_2\n    slider_3\n    slider_4\n    slider_5\n    slider_6\n    slider_7\n  }\n]",
                description: "Выдает учреждения",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/admin/getWithPar",
                type: "POST",
                param: "{\n  title\n  area\n  type\n  }",
                result: "[\n  {\n    _id\n    type\n    mainImg\n    mainTitle\n    address\n    text:{\n      title\n      text\n    }\n    coordinates\n    area\n    schoolNear:[1,2,3]\n    slider_1:{\n      title\n      slides:\n      [{\n        Name\n        type\n        hoverTxt\n        img\n      }]\n    }\n    slider_2\n    slider_3\n    slider_4\n    slider_5\n    slider_6\n    slider_7\n  }\n]",
                description: "Выдает учреждения",
                singIn: "Авторизация не требуется",
            },
            {
                url: adres + "/overlay",
                type: "POST",
                param: "{\n  }",
                result: "{\n  }",
                description: "Выдает случайное учреждение",
                singIn: "Авторизация не требуется",
            },
        ]
    });
}