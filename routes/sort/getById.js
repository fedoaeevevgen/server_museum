const Institution = require('./../../scheme/institution');
const log = require('./../../libs/logs')(module);
module.exports = async function (req, res) {
    try {
        Institution.findById(req.body._id).exec((err, result) => {
            if (err) {
                log.error(err);
                res.send([])
            } else if (result === null) {
                res.send([])
            } else {
                Institution.getInsts(result, (near) => {
                    result.schoolNear = near;
                    res.send(result);
                });
            }
        });
    }
    catch (e){
        log.error(e);
        console.log("Ошибка!!!!!");
        res.send({})
    }
};