const Institution = require('./../../scheme/institution');
const log = require('./../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        var array = [];
        if (req.body.area !== undefined) {
            for (var index in req.body.area)
                array.push(Number.parseInt(req.body.area[index]))
        }
        if (req.body.type === 'all') {
            console.log(req.body);
            console.log(array);
            Institution.find({
                status: true,
                mainTitle: {$regex: req.body.title, $options:'i'},
                area: {$in: array}
            }).exec((err, result) => {
                if (err) {
                    log.error(err);
                    res.send([])
                } else if (result === null) {
                    res.send([])
                } else {
                    res.send(result)
                }
            });
        } else {
            console.log(req.body);
            console.log(array);
            Institution.find({
                status: true,
                mainTitle: {$regex: req.body.title, $options:'i'},
                area: {$in: array},
                type: req.body.type
            }).exec((err, result) => {
                if (err) {
                    log.error(err);
                    res.send([])
                } else if (result === null) {
                    res.send([])
                } else {
                    console.log(result);
                    res.send(result);
                }
            });
        }
    }
    catch (e) {
        log.error(e);
        res.send([]);
    }
};