const institution = require('../../scheme/institution');
const log = require('../../libs/logs')(module);
const deleteFile = require('../../libs/deleteFile');
const path = require('path');
module.exports = function (req, res) {
    try{
    institution.findById(req.body._id).exec((err, result) => {
        let mass = result['slider_' + req.body.num].slides;
        console.log(req.body);
        let address = "";
        for (let i = 0; i < mass.length; i++) {
            if (mass[i].id.toString() === req.body.id.toString()) {
                address = mass[i].img;
                mass.splice(i, 1);
                break;
            }
        }
        result.set({['slider_' + req.body.num]: mass});
        console.log(path.join(__dirname,address));
        result.save((err, newInst) => {
            if (err) {
                log.error(err);
                res.send("Произошла ошибка!");
            } else {
                if(address !== "/files/name.jpg"){
                    deleteFile(path.join(__dirname,address),err => {
                        if(err){
                            log.error(err);
                            res.send("Удаление прошло успешно, но с маленькой ошибкой :)!");
                        }else{
                            res.send("Удаление прошло успешно!");
                        }
                    });
                }
                else {
                    res.send("Удаление прошло успешно!");
                }
            }
        })
    })}
    catch (e) {
        log.error(e);
        res.send("Ошибка!");
    }
};