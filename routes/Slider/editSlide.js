const institution = require('../../scheme/institution');
const saveFiles = require('../../libs/saveFiles');
const log = require('../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        institution.findById(req.body._id).exec((err, result) => {
            let mass = result['slider_' + req.body.num].slides;
            console.log(req.body);
            let object = {};
            let id = 0;
            for (let i = 0; i < mass.length; i++) {
                if (mass[i].id.toString() === req.body.id.toString()) {
                    object = mass[i];
                    id = mass[i].id;
                    mass.splice(i, 1);
                    break;
                }
            }
            let setting = {};
            for (let key in req.body) {
                if (key !== "_id")
                    if (key === "hoverTxt") {
                        object[key] = req.body[key].split('\n');
                    } else {if(key !== "_image_")
                        object[key] = req.body[key];
                    }
            }
            mass.push(object);
            result.set({['slider_' + req.body.num]: mass});
            result.save((err, newInst) => {
                if (err) {
                    log.error(err);
                    res.send("Произошла ошибка!");
                } else {
                    if (req.files[0] !== undefined) {
                        saveFiles('/', req.files[0], newInst._id + "slideid" + id + "slider_" + req.body.num, (e, f) => {
                            if (e) {
                                res.send("Человек успешно исправлен, но с фото произошла ошибка!")
                            } else {
                                let mass_2_0 = newInst["slider_" + req.body.num].slides;
                                mass_2_0[mass_2_0.length - 1].img = f;
                                let box = {...newInst["slider_" + req.body.num], slides: mass_2_0};
                                newInst.set({["slider_" + req.body.num]: box});
                                newInst.save((ee) => {
                                    if (ee) res.send("Человек успешно исправлен, но с фото произошла ошибка!");
                                    else {
                                        res.send("Человек успешно исправлен!");
                                    }
                                })
                            }
                        });
                    }
                    else {
                        res.send("Человек успешно исправлен");
                    }
                }
            })
        })
    }
    catch (e) {
        log.error(e);
        res.send("Ошибка!");
    }
};