const institution = require('../../scheme/institution');
const log = require('../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        institution.findById(req.body._id).exec((err, result) => {
            if (result === null || result === undefined) {
                res.send("Заведение не найдено!");
            } else {
                result.set({status: true});
                result.save((err) => {
                    if (err) {
                        log.error(err);
                        res.send("Произошла ошибка!");
                    }
                    else {
                        res.send("Заведения теперь видно!");
                    }
                })
            }

        })
    } catch (e) {
        log.error(e);
        res.send("Произошла ошибка");
    }
};