const institution = require('../../scheme/institution');
const log = require('../../libs/logs')(module);
module.exports = function (req, res) {
    try {
        institution.findById(req.body._id).exec((err, result) => {
            if (result === null || result === undefined) {
                res.send("Заведение не найдено!");
            } else {
                console.log(result);
                console.log(req.body);
                let textMass = [];
                let text = req.body.text;
                let title = req.body.title;
                for (let i = 0; i < text.length; i++) {
                    textMass.push({text: text[i].split('\n'), title: title[i]});
                }
                result.set({text: textMass});
                result.save((err) => {
                    if (err) {
                        log.error(err);
                        res.send("Текст заведения не был обновлен!");
                    }
                    else {
                        res.send("Текст заведения успешно изменен!");
                    }
                })
            }

        })
    } catch (e) {
        log.error(e);
        res.send("Текст заведения не был обновлен!");
    }
};