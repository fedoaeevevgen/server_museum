const Institution = require('./../../scheme/institution');

const log = require('./../../libs/logs')(module);
module.exports = async function (req, res) {
    try {
        Institution.count().exec((err, count) => {
            let random = Math.floor(Math.random() * count);
            Institution.findOne({},{type:1,mainTitle:1,mainImg:1,_id:1,text:1}).skip(random).exec(
                (err, result) => {
                    res.send(result);
                })
        })
    }
    catch (e) {
        log.error(e);
        console.log("Ошибка!!!!!");
        res.send({});
    }
};