const Institution = require('../../scheme/institution');
const saveFiles = require('../../libs/saveFiles')

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        var institution = new Institution({
            type: req.body.type,
            mainTitle: req.body.mainTitle,
            address: req.body.address,
            coordinates: req.body.coordinates,
            area: req.body.area,
            schoolNear: req.body.schoolNear,
        });
        institution.save((err, inst) => {
            if (err) {
                res.send("Учреждение не добавленно")
            }
            else {
                if (req.files[0] !== undefined)
                    saveFiles('/', req.files[0], inst._id + '', (e, f) => {
                        if (e) {
                            res.send("Учреждение успешно добавленно, но с фото произошла ошибка!")
                        } else {
                            institution.set({mainImg: f})
                            institution.save((ee) => {
                                if (ee) res.send("Учреждение успешно добавленно, но с фото произошла ошибка!")
                                else res.send("Учреждение успешно добавленно!");
                            })
                        }
                    })
                else res.send("Учреждение успешно добавленно!");
            }
        });
    }
    catch (e){
        log.error(e);
        res.send("Учреждение не добавленно")
    }
};
