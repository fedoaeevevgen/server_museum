const Institution = require('./../../../scheme/institution');
const log = require('./../../../libs/logs')(module);
module.exports = async function (req, res) {
    try {
        Institution.findById(req.body._id).exec((err, result) => {
            if (err) {
                log.error(err);
                res.send([])
            } else if (result === null) {
                res.send([])
            } else {
                res.send(result)
            }
        });
    }
    catch {
        console.log("Ошибка!!!!!");
        res.send("Учреждение не добавленно")
    }
};