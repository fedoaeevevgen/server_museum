const veteran = require('../../scheme/veteran');
const log = require('../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        veteran.findById(req.body._id).exec((err, result) => {
            let id = 0;
            for (var index in result.media.video) {
                if (result.media.video[index].id >= id) {
                    id = result.media.video[index].id;
                }
            }
            id++;
            if (req.body.address !== undefined) {
                let mass = result.media;
                mass.video.push({
                    value: req.body.address,
                    id: id
                }) ;
                result.set({media: mass});
                result.save((ee) => {
                    if (ee) res.send("Ошибка сохранения!");
                    else res.send("Видео успешно добавленно к ветерану!");
                })
            } else {
                res.send("Ошибка!");
            }
        });
    }
    catch (err) {
        log.error(err);
        res.send(err);
    }
};
