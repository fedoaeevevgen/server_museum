const veteran = require('../../scheme/veteran');
const saveFiles = require('../../libs/saveFiles');
const log = require('../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        veteran.findById(req.body._id).exec((err, result) => {
            let id = 0;
            for (var index in result.media.img) {
                if (result.media.img[index].id >= id) {
                    id = result.media.img[index].id;
                }
            }
            id++;
            if (req.files[0] !== undefined) {
                saveFiles('/', req.files[0], result._id + 'photoVet' + id, (e, f) => {
                    if (e) {
                        res.send("Ветеран успешно изменен, но с фото произошла ошибка!")
                    } else {
                        let mass = result.media;
                        mass.img.push({
                            value: f,
                            id: id
                        });
                        result.set({media: mass});
                        result.save((ee) => {
                            if (ee) res.send("Ветеран успешно изменен, но с фото произошла ошибка!");
                            else res.send("Ветеран успешно изменен!");
                        })
                    }
                });
            } else {
                res.send("Ошибка!");
            }
        });
    }
    catch (err) {
        log.error(err);
        res.send(err);
    }
};
