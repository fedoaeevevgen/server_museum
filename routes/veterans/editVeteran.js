const veteran = require('../../scheme/veteran');
const saveFiles = require('../../libs/saveFiles');
const log = require('../../libs/logs')(module);

module.exports = async function (req, res) {
    try {
        console.log(req.body);
        veteran.findById(req.body._id).exec((err, result) => {
            if (result === null) {
                res.send("Ветеран не найден!")
            } else {
                result.set({miniInfo: req.body.miniInfo.split('\n')});

                result.set({text: req.body.text.split('\n')});

                result.set({name: req.body.name});
                if (req.files[0] !== undefined) {
                    saveFiles('/', req.files[0], result._id + 'photoVetMain', (e, f) => {
                        if (e) {
                            res.send("Ошибка!")
                        } else {
                            result.set({mainPhoto: f});
                            result.save((ee) => {
                                if (ee) res.send("Ошибка!");
                                else res.send("Фото успешно изменено!");
                            })
                        }
                    });
                } else {
                    result.save((ee) => {
                        if (ee) res.send("Ошибка!");
                        else res.send("Изменения прошли успешно!");
                    })
                }
            }

        });
    }
    catch (err) {
        log.error(err);
        res.send("Ошибка!");
    }
};
