const veteran = require('./../../scheme/veteran');
const log = require('./../../libs/logs')(module);
module.exports = async function (req, res) {
    try {
        veteran.findById(req.body._id).exec((err, result) => {
            if (err) {
                log.error(err);
                res.send({})
            } else if (result === null) {
                res.send({})
            } else {
                res.send(result)
            }
        });
    }
    catch (e){
        log.error(e);
        console.log("Ошибка!!!!!");
        res.send({})
    }
};