﻿const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const compression = require('compression');
const multer = require('multer');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const path = require('path');
const config = require('./libs/config');
const log = require('./libs/logs')(module);

mongoose.connect(config.mongoose.url, { useNewUrlParser: true , useCreateIndex: true }, function (err) {
if (err) 
    return console.log(err);  
const app = express();
app.use(cookieParser());
    app.use('/', express.static('./views/public'));
app.use(compression());
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(multer(
    {
        dest: path.join(__dirname, 'public/uploads'),
        limits: {
        }
    }
).any());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});
app.use('/files', express.static('./files'));
require('./routes')(app);
app.use(function(req, res){
    res.redirect('api');
});
var httpServer = http.createServer(app);
function onListening(){
    log.info('Listening on port ', config.port);
}
httpServer.on('listening', onListening);
httpServer.listen(config.port,config.adress);
});
