const mongoose = require('mongoose');
const crypto = require('crypto');


const slider = mongoose.Schema({
    title: String,
    slides: Array
});

const institutionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    status: {
        type: Boolean,
        default: function () {
            return true;
        },
    },
    type: {
        required: true,
        type: String,
    },
    mainImg: {
        type: String,
        default: "/files//name.jpg"
    },
    mainTitle: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    text: {
        type: Array,
        default: []
    },
    coordinates: {
        type: Array,
        required: true
    },
    area: {
        type: Number,
        required: true
    },
    schoolNear: {
        type: Array,
        default: []
    },
    slider_1: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_2: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_3: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_4: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_5: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_6: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
    slider_7: {
        title: String,
        slides: [{
            Name: String,
            hoverTxt: [String],
            id: Number,
            type1: String,
            img: String
        }
        ]
    },
}, {
    collection: "institution",
    versionKey: false
});

function qwe(inst) {
    return new Promise((resolve, reject) => {
        mongoose.model('institution', institutionSchema).findById(inst,
            {mainTitle: 1,_id:1,mainImg:1,coordinates:1})
            .exec((err, res) => {
                console.log(res);
                resolve(res);
            });
    })
}


institutionSchema.statics = {
    getInsts: function (inst,callback) {
        Promise.all(inst.schoolNear.map((item)=> qwe(item)))
            .then(results => {
                console.log(results);
                callback(results);
            });
    }
};

institutionSchema.pre('save', function (next) {
    if (this._id === null || this._id === undefined)
        this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('institution', institutionSchema);
