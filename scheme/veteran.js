const mongoose = require('mongoose');


const veteranSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    status: {
        type: Boolean,
        default: () => true,
    },
    name:{
        type:String
    },
    mainPhoto: {
        type: String,
        default: "/files//name.jpg"
    },
    miniInfo: {
        type: String,
        default: () => ""
    },
    title: {
        type: String,
        default: () => "Биография"
    },
    text: {
        type: Array,
    },
    media: {
        img: [{
            id: Number,
            value: String
        }],
        video: [{
            id: Number,
            value: String
        }]
    },
}, {
    collection: "veteran",
    versionKey: false
});


veteranSchema.pre('save', function (next) {
    if (this._id === null || this._id === undefined)
        this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('veteran', veteranSchema);
